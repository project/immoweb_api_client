# IMMOWEB API CLIENT

This module provides an API Client for Immoweb.be making it easier for
developing custom requirements in order to integrate with Immoweb.

This module is aimed towards developers and provides no real functionality on
its own other as providing a configuration form and defining all the services,
so you can easily integrate Immoweb into your project.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/immoweb_api_client).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/immoweb_api_client).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.

Minimal PHP requirement is `PHP 8.0`.


## Installation

Install the Immoweb API Client as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


## Configuration

1. Configure the user permissions in Administration » People » Permissions:
   administer immoweb api client

2. Customize the Immoweb settings in `Administration » Configuration » Services » Immoweb API Client`.

3. To configure the module you'll need to gather some information by contacting
   your contact person of Immoweb. They will provide you the necessary
   credentials and API keys.

4. Once you have all the credentials you can enter them in the configuration
   form which is located at: /admin/config/services/immoweb_api_client


## Maintainers

- Bram Driesen - [BramDriesen](https://www.drupal.org/u/bramdriesen)
