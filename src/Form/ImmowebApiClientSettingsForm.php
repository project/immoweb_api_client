<?php

namespace Drupal\immoweb_api_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface;
use Drupal\immoweb_api_client\Service\ClassifiedInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImmowebApiClientSettingsForm.
 *
 * Provides all configuration for the Immoweb API Client.
 */
class ImmowebApiClientSettingsForm extends ConfigFormBase {

  /**
   * The authenticator interface.
   *
   * @var \Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface
   */
  protected AuthenticatorInterface $authService;

  /**
   * The classified service.
   *
   * @var \Drupal\immoweb_api_client\Service\ClassifiedInterface
   */
  protected ClassifiedInterface $classifiedService;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'immoweb_api_client.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'immoweb_api_client_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->authService = $container->get('immoweb_api_client.oath');
    $instance->classifiedService = $container->get('immoweb_api_client.classified');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('immoweb_api_client.settings');

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#description' => $this->t('The client ID for the authorization request. Normally the default value "iw_api_in" is correct.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => empty($config->get('client_id')) ? 'iw_api_in' : $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#description' => $this->t('The client Secret for the authorization request.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('The username for the authorization request.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('username'),
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The password for the authorization request.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('password'),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The client API key for the classified request.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];

    $form['target_server'] = [
      '#type' => 'radios',
      '#title' => $this->t('Target Server'),
      '#description' => $this->t('The target server you want to connect to. Useful when developing your integration, can be changed in your settings.php file per environment.'),
      '#options' => [
        'DEV' => $this->t('DEV Environment'),
        'TEST' => $this->t('TEST Environment'),
        'PROD' => $this->t('PROD Environment'),
      ],
      '#default_value' => $config->get('target_server') ?: 'TEST',
      '#required' => TRUE,
      'DEV' => [
        '#disabled' => TRUE,
      ],
    ];

    // If the form is loaded, and we have configured values we test those
    // settings by making an authentication request.
    if (!empty($config->get('password'))) {
      $this->testOauthTokenRequest();
      $this->testClassifiedRequest();
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->deleteAll();
    parent::submitForm($form, $form_state);

    $this->config('immoweb_api_client.settings')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('target_server', $form_state->getValue('target_server'))
      ->save();

    // Clear the cache of the authenticator.
    $this->authService->invalidateTokenInformation();
  }

  /**
   * Function to test the configured settings for the authentication.
   *
   * This is to ensure that with the configured settings the connection can be
   * established and to ensure the authentication actually succeeds.
   */
  protected function testOauthTokenRequest(): void {
    try {
      $this->authService->oauthTokenRequest();
      // If no exception is thrown our request succeeded.
      $this->messenger()->addMessage($this->t('Oauth token request succeeded.'));
    }
    catch (GuzzleException $exception) {
      $this->messenger()->addError($this->t('Oauth token request failed (bad credentials). Please validate the: Client ID, Client Secret, Username and password.'));
    }
  }

  /**
   * Function to test the configured API key for the classifieds request.
   *
   * This is to verify that we are able to make an authenticated request with
   * our configured oauth credentials and our API key for the classified
   * request.
   */
  protected function testClassifiedRequest() {
    try {
      $this->classifiedService->getClassified(666);
    }
    catch (GuzzleException $exception) {
      // Maybe a bit dirty, but we check only if our returned code is not a bad
      // request (400), unauthorized (401) or forbidden (403).
      if ($exception->getCode() !== 400 && $exception->getCode() !== 401 && $exception->getCode() !== 403) {
        $this->messenger()->addMessage($this->t('Classified request succeeded.'));
        return;
      }

      $this->messenger()->addError($this->t('Classified request failed. Please verify the API Key and Oauth credentials.'));
    }
  }

}
