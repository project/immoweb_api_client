<?php

namespace Drupal\immoweb_api_client\Helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\immoweb_api_client\Service\ClassifiedInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Immoweb helper class containing generic helper functions.
 */
class ImmowebHelper implements ImmowebHelperInterface, ContainerInjectionInterface {

  /**
   * The Immoweb base URLS.
   */
  const IMMOWEB_BASE_URLS = [
    'DEV' => 'https://www.test.immoweb.be/',
    'TEST' => 'https://www.test.immoweb.be/',
    'PROD' => 'https://www.immoweb.be/',
  ];

  /**
   * The Immoweb Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The Immoweb classified interface.
   *
   * @var \Drupal\immoweb_api_client\Service\ClassifiedInterface
   */
  protected ClassifiedInterface $classified;

  /**
   * ImmoweHelper constructor.
   */
  public function __construct(ConfigFactoryInterface $config, ClassifiedInterface $classified) {
    $this->config = $config->get('immoweb_api_client.settings');
    $this->classified = $classified;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('immoweb_api_client.classified')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getImmowebUrl(): ?string {
    if (!empty($this->config->get('target_server')) && isset(static::IMMOWEB_BASE_URLS[$this->config->get('target_server')])) {
      return static::IMMOWEB_BASE_URLS[$this->config->get('target_server')];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getClassifiedUrl(string $referenceId, string $language = 'en'): string {
    $data = $this->classified->getClassifiedStatus($referenceId);

    if ($data === NULL) {
      return '';
    }

    // Decode the json object.
    $classified = json_decode($data->getBody()->getContents());

    // Exit early if there was no valid response on which we could build the
    // result URL.
    if (!isset($classified->classifiedState) || !isset($classified->classifiedState->legacyId)) {
      return '';
    }

    // Fetch the Immoweb website url.
    $base_url = $this->getImmowebUrl();

    // Fallback to the production URL.
    if ($base_url === NULL) {
      $base_url = static::IMMOWEB_BASE_URLS['PROD'];
    }

    return match ($language) {
      'nl' => $base_url . $language . '/zoekertje/' . $classified->classifiedState->legacyId,
      'fr' => $base_url . $language . '/annonce/' . $classified->classifiedState->legacyId,
      default => $base_url . $language . '/classified/' . $classified->classifiedState->legacyId,
    };
  }

}
