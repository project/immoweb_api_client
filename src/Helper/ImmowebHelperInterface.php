<?php

namespace Drupal\immoweb_api_client\Helper;

/**
 * Interface class for the helper class.
 */
interface ImmowebHelperInterface {

  /**
   * Returns the public facing website URL based on the configured back-end.
   *
   * @return string|null
   *   The website URL. Null in case no target back-end is selected.
   */
  public function getImmowebUrl(): ?string;

  /**
   * Method to get the public Immoweb url to the given classified.
   *
   * @param string $referenceId
   *   The reference id of the classified.
   * @param string $language
   *   (optional) the language in which the URL should be returned.
   *   This defaults to 'en'. Allowed values are 'en', 'nl', 'fr'.
   *
   * @return string
   *   The public URL for the classified.
   *   Empty string in case of error when fetching the object from Immoweb.
   */
  public function getClassifiedUrl(string $referenceId, string $language = 'en'): string;

}
