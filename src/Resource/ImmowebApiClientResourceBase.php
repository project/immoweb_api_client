<?php

namespace Drupal\immoweb_api_client\Resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImmowebApiClientResourceBase.
 *
 * Resource base for all endpoint implementations.
 *
 * @package Drupal\immoweb_api_client\Resource
 */
abstract class ImmowebApiClientResourceBase implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Immoweb Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The logger interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Our Authentication class.
   *
   * @var \Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface
   */
  protected AuthenticatorInterface $authenticator;

  /**
   * The endpoint base urls.
   *
   * If one of the environments happens to be unavailable for the resource it's
   * recommended to mark it as NULL.
   *
   * @var array
   *   An array of URLS.
   */
  protected array $endpointBaseUrls;

  /**
   * ImmowebApiClientResourceBase constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory interface.
   * @param \Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface $authenticate
   *   The authenticator (oauth) interface.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config, LoggerChannelFactoryInterface $logger_factory, AuthenticatorInterface $authenticate) {
    $this->httpClient = $http_client;
    $this->config = $config->get('immoweb_api_client.settings');
    $this->logger = $logger_factory->get('immoweb_api_client');
    $this->authenticator = $authenticate;
    // Used to enforce that all classes extending this class define the static
    // base urls. If the static value is not defined it will throw a fatal
    // error.
    $this->endpointBaseUrls = static::IMMOWEB_ENDPOINT_BASE_URLS;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('immoweb_api_client.oath')
    );
  }

  /**
   * Gets the base URL of the URLS defined in the child class based on the env.
   *
   * @return mixed|null
   *   The URL if found, otherwise NULL.
   */
  public function getBaseUrl(): ?string {
    if (!empty($this->config->get('target_server')) && isset($this->endpointBaseUrls[$this->config->get('target_server')])) {
      return $this->endpointBaseUrls[$this->config->get('target_server')];
    }
    return NULL;
  }

  /**
   * Helper function to validate parameters.
   *
   * @param array $validation_type
   *   An structured array defining the type of check for each passed variable.
   *   Currently supported:
   *   - 'not_empty': Performs an empty() check and returns false if empty.
   * @param mixed $variables
   *   The variable we want to validate against our validation type.
   *
   * @return bool
   *   A boolean indication if our validation passed or not.
   */
  public function validateParameters(array $validation_type, ...$variables): bool {
    foreach ($variables as $key => $variable) {
      switch ($validation_type[$key]) {
        case 'not_empty':
          if (empty($variable)) {
            $this->logger->error('Some of the parameters are empty but a value is required.');
            return FALSE;
          }
          break;
      }
    }

    return TRUE;
  }

}
