<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ClassifiedCreate.
 *
 * Class containing all the logic to create a classified.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
class ClassifiedCreate extends ClassifiedRequestBase implements ClassifiedCreateInterface {

  /**
   * Indication of the validation passed or not.
   *
   * @var bool
   */
  protected bool $validation = TRUE;

  /**
   * {@inheritdoc}
   */
  public function createClassified(array $body): ?ResponseInterface {
    // Validation.
    if (!$this->validateParameters(['not_empty'], $body) || !$this->validateBody($body)) {
      $this->logger->error($this->t('Not all required parameters passed validation.'));
      return NULL;
    }

    // Make the request.
    try {
      return $this->httpClient->request(
        'POST',
        $this->getBaseUrl() . '/upsert',
        [
          'body' => Json::encode($body),
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => $this->authenticator->getToken(),
            'x-iw-api-key' => $this->config->get('api_key'),
          ],
        ],
      );
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getResponse()->getBody()->getContents());
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
    catch (GuzzleException $exception) {
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
  }

  /**
   * Function which will validate the request body against the schema.
   *
   * Will only check if the keys are present. No type check is being done on the
   * value of the keys.
   *
   * @param array $body
   *   The body array.
   *
   * @return bool
   *   Indication if the body is valid or not.
   */
  private function validateBody(array $body): bool {
    $schema = [
      'externalId' => 'string',
      'property' => [
        'type' => 'string',
        'subtype' => 'string',
        'location' => [
          'address' => [
            'country' => 'string',
            'postalCode' => 'string',
            'street' => 'string',
            'number' => 'string',
          ],
        ],
      ],
      'transaction' => [
        'type' => 'string',
        'subtype' => 'string',
      ],
      'publication' => [
        'isActive' => 'bool',
      ],
    ];

    $this->recursiveCheck($schema, $body);

    return $this->validation;
  }

  /**
   * Function to recursively check all array elements if they are set.
   *
   * Will also perform type checking.
   *
   * @param array $schema
   *   The schema to validate the data against.
   * @param array $data
   *   The data to be validated against the schema.
   */
  private function recursiveCheck(array $schema, array $data): void {
    foreach ($schema as $key => $value) {
      // If the key is not found, we skip any further validation and mark the
      // validation as failed.
      if (!isset($data[$key])) {
        $this->validation = FALSE;
        return;
      }

      // If we have an array we do a recursive check.
      if (is_array($value)) {
        $this->recursiveCheck($schema[$key], $data[$key]);
        continue;
      }

      // Dynamic type checking.
      $this->validation = match ($value) {
        'bool', 'boolean' => is_bool($data[$key]),
        'int', 'integer' => is_int($data[$key]),
        'float', 'double' => is_float($data[$key]),
        'string' => is_string($data[$key]),
        'default' => FALSE,
      };
    }
  }

}
