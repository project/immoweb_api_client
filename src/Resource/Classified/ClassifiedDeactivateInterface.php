<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface ClassifiedDeactivateInterface.
 *
 * Interface class for all Deactivate Classified Request methods.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
interface ClassifiedDeactivateInterface {

  /**
   * Deactivates a classified.
   *
   * @param string $external_id
   *   The external ID of the classified.
   *
   * @see: https://developer.immoweb.be/resources/classifieds/pipeline/2#operations-Classified_Requests-createRequest
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The HTTP client response or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function deactivateClassified(string $external_id): ?ResponseInterface;

}
