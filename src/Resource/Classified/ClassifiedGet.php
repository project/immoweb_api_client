<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ClassifiedGet.
 *
 * Class containing all the logic to get classified information.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
class ClassifiedGet extends ClassifiedRequestBase implements ClassifiedGetInterface {

  /**
   * {@inheritdoc}
   */
  public function getClassified(string $reference_id): ?ResponseInterface {
    // Validation.
    if (!$this->validateParameters(['not_empty'], $reference_id)) {
      $this->logger->error($this->t('Not all required parameters passed validation.'));
      return NULL;
    }

    // Make the request.
    try {
      return $this->httpClient->request(
        'GET',
        $this->getBaseUrl() . '/' . $reference_id,
        [
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => $this->authenticator->getToken(),
            'x-iw-api-key' => $this->config->get('api_key'),
          ],
        ]
      );
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getResponse()->getBody()->getContents());
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
    catch (GuzzleException $exception) {
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getClassifiedStatus(string $reference_id): ?ResponseInterface {
    // Validation.
    if (!$this->validateParameters(['not_empty'], $reference_id)) {
      $this->logger->error($this->t('Not all required parameters passed validation.'));
      return NULL;
    }

    // Make the request.
    try {
      return $this->httpClient->request(
        'GET',
        $this->getBaseUrl() . '/' . $reference_id . '/status',
        [
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => $this->authenticator->getToken(),
            'x-iw-api-key' => $this->config->get('api_key'),
          ],
        ]
      );
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getResponse()->getBody()->getContents());
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
    catch (GuzzleException $exception) {
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
  }

}
