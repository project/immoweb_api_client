<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface ClassifiedGetInterface.
 *
 * Interface class for all Get Classified Request methods.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
interface ClassifiedGetInterface {

  /**
   * Gets a classified.
   *
   * @param string $reference_id
   *   The reference ID of the classified (This is returned after a creation).
   *
   * @see: https://developer.immoweb.be/resources/classifieds/pipeline/2#operations-Classified_Requests-getRequest
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The HTTP client response or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getClassified(string $reference_id): ?ResponseInterface;

  /**
   * Gets the status of a classified.
   *
   * @param string $reference_id
   *   The reference ID of the classified (This is returned after a creation).
   *
   * @see: https://developer.immoweb.be/resources/classifieds/pipeline/2#operations-Classified_Requests-getRequestStatus
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The HTTP client response or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getClassifiedStatus(string $reference_id): ?ResponseInterface;

}
