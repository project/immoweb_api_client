<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface ClassifiedCreateInterface.
 *
 * Interface class for all Create Classified Request methods.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
interface ClassifiedCreateInterface {

  /**
   * Creates a classified.
   *
   * @param array $body
   *   An array matching the schema ClassifiedRequest.
   *
   * @see: https://developer.immoweb.be/resources/classifieds/pipeline/2#operations-Classified_Requests-createRequest
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The HTTP client response or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createClassified(array $body): ?ResponseInterface;

}
