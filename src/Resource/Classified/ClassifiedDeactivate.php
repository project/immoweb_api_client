<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ClassifiedDeactivate.
 *
 * Class containing all the logic to deactivate a classified.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
class ClassifiedDeactivate extends ClassifiedRequestBase implements ClassifiedDeactivateInterface {

  /**
   * {@inheritdoc}
   */
  public function deactivateClassified(string $external_id): ?ResponseInterface {
    // Validation.
    if (!$this->validateParameters(['not_empty'], $external_id)) {
      $this->logger->error($this->t('Not all required parameters passed validation.'));
      return NULL;
    }

    // Make the request.
    try {
      return $this->httpClient->request(
        'POST',
        $this->getBaseUrl() . '/deactivate',
        [
          'body' => Json::encode(['externalId' => $external_id]),
          'headers' => [
            'Content-Type' => 'application/json',
            'Authorization' => $this->authenticator->getToken(),
            'x-iw-api-key' => $this->config->get('api_key'),
          ],
        ]
      );
    }
    catch (RequestException $exception) {
      $this->logger->error($exception->getResponse()->getBody()->getContents());
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
    catch (GuzzleException $exception) {
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
  }

}
