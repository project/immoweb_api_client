<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use Drupal\immoweb_api_client\Resource\ImmowebApiClientResourceBase;

/**
 * Class ClassifiedRequestBase.
 *
 * Base class in order to only set the URL in one place.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
class ClassifiedRequestBase extends ImmowebApiClientResourceBase {

  const IMMOWEB_ENDPOINT_BASE_URLS = [
    'DEV' => 'https://classifieds.dev.immowebapi.be/pipeline/3',
    'TEST' => 'https://classifieds.test.immowebapi.be/pipeline/3',
    'PROD' => 'https://classifieds.immowebapi.be/pipeline/3',
  ];

}
