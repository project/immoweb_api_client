<?php

namespace Drupal\immoweb_api_client\Resource\Classified;

use Psr\Http\Message\ResponseInterface;

/**
 * Class ClassifiedUpdate.
 *
 * Class containing all the logic to update a classified.
 *
 * @package Drupal\immoweb_api_client\Resource\Classified
 */
class ClassifiedUpdate extends ClassifiedCreate implements ClassifiedUpdateInterface {

  /**
   * {@inheritdoc}
   */
  public function updateClassified(array $body): ?ResponseInterface {
    // Update uses the same post method as the create resource for now.
    return $this->createClassified($body);
  }

}
