<?php

namespace Drupal\immoweb_api_client\Resource\Customer;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface AuthenticatorInterface.
 *
 * Interface for our authenticator class.
 *
 * @package Drupal\immoweb_api_client\Resource\Customer
 */
interface AuthenticatorInterface {

  /**
   * Oauth authentication based on the values set in the configuration.
   *
   * @see: https://developer.immoweb.be/resources/customers/authentication/1
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The HTTP client response or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function oauthTokenRequest(): ?ResponseInterface;

  /**
   * Oauth authentication with the refresh token.
   *
   * @param string $refresh_token
   *   The refresh token to complete the request with.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The HTTP client response or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function oauthTokenRefreshRequest(string $refresh_token): ?ResponseInterface;

  /**
   * Function to get the actual bearer token.
   *
   * Will request a new token if it doesn't exist.
   * Will request a new token if it's expired.
   *
   * Token is cached until it expires.
   *
   * @return string
   *   The bearer token in the format of 'bearer TOKEN'.
   */
  public function getToken(): ?string;

  /**
   * Invalidate the cached token information.
   */
  public function invalidateTokenInformation(): void;

}
