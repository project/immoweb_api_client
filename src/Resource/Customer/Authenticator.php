<?php

namespace Drupal\immoweb_api_client\Resource\Customer;

use DateTime;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Authenticator.
 *
 * Class responsible for getting an authentication object. This class does not
 * extend the ImmowebApiClientResourceBase class because we would otherwise end
 * up with an infinite loop since this authenticator class is being used for all
 * other resources that require the Oauth token.
 *
 * @package Drupal\immoweb_api_client\Resource\Customer.
 */
class Authenticator implements AuthenticatorInterface, ContainerInjectionInterface {

  const IMMOWEB_ENDPOINT_BASE_URLS = [
    'DEV' => 'https://connect.dev.immowebapi.be/customers',
    'TEST' => 'https://connect-test.immoweb.be/customers',
    'PROD' => 'https://connect.immoweb.be/customers',
  ];

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The Immoweb Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The logger interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The 'cache.default' cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * The endpoint base urls.
   *
   * If one of the environments happens to be unavailable for the resource it's
   * recommended to mark it as NULL.
   *
   * @var array
   *   An array of URLS.
   */
  protected array $endpointBaseUrls;

  /**
   * The Immoweb access token.
   *
   * @var string
   */
  private string $accessToken;

  /**
   * The Immoweb refresh token.
   *
   * @var string
   */
  private string $refreshToken;

  /**
   * The timestamp when our token expires.
   *
   * @var int
   */
  private int $expiration;

  /**
   * Authenticate constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The Drupal cache backend.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config, LoggerChannelFactoryInterface $logger_factory, CacheBackendInterface $cache_backend) {
    $this->httpClient = $http_client;
    $this->config = $config->get('immoweb_api_client.settings');
    $this->logger = $logger_factory->get('immoweb_api_client');
    $this->cacheBackend = $cache_backend;
    // Used to enforce that all classes extending this class define the static
    // base urls. If the static value is not defined it will throw a fatal
    // error.
    $this->endpointBaseUrls = static::IMMOWEB_ENDPOINT_BASE_URLS;
    // Load values from cache.
    $this->getTokenInformationFromCache();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function oauthTokenRequest(): ?ResponseInterface {
    try {
      return $this->httpClient->request(
        'POST',
        $this->getBaseUrl() . '/oauth/token',
        [
          'form_params' => [
            'grant_type' => 'password',
            'client_id' => $this->config->get('client_id'),
            'client_secret' => $this->config->get('client_secret'),
            'username' => $this->config->get('username'),
            'password' => $this->config->get('password'),
          ],
          'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
          ],
        ]
      );
    }
    catch (GuzzleException $exception) {
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function oauthTokenRefreshRequest(string $refresh_token): ?ResponseInterface {
    try {
      return $this->httpClient->request(
        'POST',
        $this->getBaseUrl() . '/oauth/token',
        [
          'form_params' => [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'client_id' => $this->config->get('client_id'),
            'client_secret' => $this->config->get('client_secret'),
          ],
          'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
          ],
        ]
      );
    }
    catch (GuzzleException $exception) {
      $this->logger->error($exception->getMessage());
      throw $exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getToken(): ?string {
    $expired = FALSE;
    if (!empty($this->expiration)) {
      // Return our token if it's not empty and not expired.
      $expired = new DateTime() > new DateTime('@' . $this->expiration);
      if (!empty($this->accessToken) && !$expired) {
        return $this->accessToken;
      }
    }

    // Here we can assume either out token is empty or expired, thus request a
    // new token.
    if ($expired && !empty($this->refreshToken)) {
      // Needs to be wrapped in a try catch because if the refresh token is also
      // expired it will throw an exception.
      try {
        $token_request = $this->oauthTokenRefreshRequest($this->refreshToken);
      }
      catch (GuzzleException $exception) {
        $token_request = NULL;
      }
    }

    // If our token request is empty or if our token request for the refresh
    // token didn't return a 200 code, we request a fresh one.
    if (empty($token_request) || $token_request->getStatusCode() !== 200) {
      $token_request = $this->oauthTokenRequest();
    }

    // Check if any of our token requests was empty or din't reutnr a 200 code.
    if (!empty($token_request) && $token_request->getStatusCode() === 200) {
      $body = json_decode($token_request->getBody()->getContents(), FALSE);
      $this->accessToken = 'Bearer ' . $body->access_token;
      $this->refreshToken = $body->refresh_token;
      $expiration = new DateTime('+' . $body->expires_in . ' seconds');
      $this->expiration = $expiration->getTimestamp();
      // Cache our token information.
      $this->cacheTokenInformation();
      return $this->accessToken;
    }

    // If we end up here that means we failed to successfully return or obtain a
    // access token.
    $this->logger->error('Failed to fetch an access token. Check the logs for more information.');
    return NULL;
  }

  /**
   * Gets the base URL of the URLS defined in the child class based on the env.
   *
   * @return mixed|null
   *   The URL if found, otherwise NULL.
   */
  private function getBaseUrl(): ?string {
    if (!empty($this->config->get('target_server')) && isset($this->endpointBaseUrls[$this->config->get('target_server')])) {
      return $this->endpointBaseUrls[$this->config->get('target_server')];
    }
    return NULL;
  }

  /**
   * Sets all the tokens in our class ready to be used for other functions.
   */
  private function getTokenInformationFromCache(): void {
    $access_token = $this->cacheBackend->get('immoweb_api_client_access_token');
    $refresh_token = $this->cacheBackend->get('immoweb_api_client_refresh_token');
    $expiration = $this->cacheBackend->get('immoweb_api_client_expiration');

    if ($access_token && $refresh_token && $expiration) {
      $this->accessToken = $access_token->data;
      $this->refreshToken = $refresh_token->data;
      $this->expiration = $expiration->data;
    }
  }

  /**
   * Stores all token information in the Drupal cache backend.
   */
  private function cacheTokenInformation(): void {
    $this->cacheBackend->setMultiple(
      [
        'immoweb_api_client_access_token' => [
          'data' => $this->accessToken,
        ],
        'immoweb_api_client_refresh_token' => [
          'data' => $this->refreshToken,
        ],
        'immoweb_api_client_expiration' => [
          'data' => $this->expiration,
        ],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTokenInformation(): void {
    $this->cacheBackend->invalidateMultiple(
      [
        'immoweb_api_client_access_token',
        'immoweb_api_client_refresh_token',
        'immoweb_api_client_expiration',
      ]
    );
  }

}
