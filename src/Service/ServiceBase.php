<?php

namespace Drupal\immoweb_api_client\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ServiceBase.
 *
 * Base class implementing the general dependency injections.
 *
 * @package Drupal\immoweb_api_client\Service
 */
abstract class ServiceBase implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Immoweb Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The logger interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Our Authentication class.
   *
   * @var \Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface
   */
  private AuthenticatorInterface $authenticator;

  /**
   * ServiceBase constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory interface.
   * @param \Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface $authenticate
   *   The authenticator (oauth) interface.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config, LoggerChannelFactoryInterface $logger_factory, AuthenticatorInterface $authenticate) {
    $this->httpClient = $http_client;
    $this->config = $config->get('immoweb_api_client.settings');
    $this->logger = $logger_factory->get('immoweb_api_client');
    $this->authenticator = $authenticate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('immoweb_api_client.oath')
    );
  }

}
