<?php

namespace Drupal\immoweb_api_client\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedCreate;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedDeactivate;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedDelete;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedGet;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedUpdate;
use Drupal\immoweb_api_client\Resource\Customer\AuthenticatorInterface;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Classified.
 *
 * Responsible for grouping all resources under the Classified collection.
 */
class Classified extends ServiceBase implements ClassifiedInterface {

  /**
   * The classified create class.
   *
   * @var \Drupal\immoweb_api_client\Resource\Classified\ClassifiedCreate
   */
  private ClassifiedCreate $create;

  /**
   * The classified deactivate class.
   *
   * @var \Drupal\immoweb_api_client\Resource\Classified\ClassifiedDeactivate
   */
  private ClassifiedDeactivate $deactivate;

  /**
   * The classified delete class.
   *
   * @var \Drupal\immoweb_api_client\Resource\Classified\ClassifiedDelete
   */
  private ClassifiedDelete $delete;

  /**
   * The classified get class.
   *
   * @var \Drupal\immoweb_api_client\Resource\Classified\ClassifiedGet
   */
  private ClassifiedGet $get;

  /**
   * The classified get class.
   *
   * @var \Drupal\immoweb_api_client\Resource\Classified\ClassifiedUpdate
   */
  private ClassifiedUpdate $update;

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config, LoggerChannelFactoryInterface $logger_factory, AuthenticatorInterface $authenticate) {
    parent::__construct($http_client, $config, $logger_factory, $authenticate);
    $this->create = new ClassifiedCreate($http_client, $config, $logger_factory, $authenticate);
    $this->deactivate = new ClassifiedDeactivate($http_client, $config, $logger_factory, $authenticate);
    $this->delete = new ClassifiedDelete($http_client, $config, $logger_factory, $authenticate);
    $this->get = new ClassifiedGet($http_client, $config, $logger_factory, $authenticate);
    $this->update = new ClassifiedUpdate($http_client, $config, $logger_factory, $authenticate);
  }

  /**
   * {@inheritdoc}
   */
  public function createClassified(array $body): ?ResponseInterface {
    return $this->create->createClassified($body);
  }

  /**
   * {@inheritdoc}
   */
  public function updateClassified(array $body): ?ResponseInterface {
    return $this->update->updateClassified($body);
  }

  /**
   * {@inheritdoc}
   */
  public function deactivateClassified(string $external_id): ?ResponseInterface {
    return $this->deactivate->deactivateClassified($external_id);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteClassified(string $external_id): ?ResponseInterface {
    return $this->delete->deleteClassified($external_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getClassified(string $reference_id): ?ResponseInterface {
    return $this->get->getClassified($reference_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getClassifiedStatus(string $reference_id): ?ResponseInterface {
    return $this->get->getClassifiedStatus($reference_id);
  }

}
