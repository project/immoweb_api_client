<?php

namespace Drupal\immoweb_api_client\Service;

use Drupal\immoweb_api_client\Resource\Classified\ClassifiedCreateInterface;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedDeactivateInterface;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedDeleteInterface;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedGetInterface;
use Drupal\immoweb_api_client\Resource\Classified\ClassifiedUpdateInterface;

/**
 * Interface ClassifiedInterface.
 *
 * The interface class for the Classified service. Doesn't define any functions
 * on it's own for now.
 *
 * @package Drupal\immoweb_api_client\Service
 */
interface ClassifiedInterface extends
    ClassifiedCreateInterface,
    ClassifiedDeactivateInterface,
    ClassifiedDeleteInterface,
    ClassifiedGetInterface,
    ClassifiedUpdateInterface {

}
